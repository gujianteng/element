import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Layout from '@/components/Layout'
import Button from '@/components/Button'
import Form from '@/components/Form'
import Icon from '@/components/Icom'
import Input from '@/components/Input'
import Radio from '@/components/Radio'
import Checkbox from '@/components/Checkbox'
import Switch from '@/components/Switch'
import Select from '@/components/Select'
import Cascader from '@/components/Cascader'
import DatePicker from '@/components/DatePicker'
import Validator from '@/components/Validator'
import Form2 from '@/components/Form2'
import Form3 from '@/components/Form3'
import Table from '@/components/Table'
import Pagination from '@/components/Pagination'
import Dialog from '@/components/Dialog'
import Card from '@/components/Card'
import Breadcrumb from '@/components/Breadcrumb'
import Steps from '@/components/Steps'
import NavMenu from '@/components/NavMenu'
import Avatar from '@/components/Avatar'
import Dropdown from '@/components/Dropdown'
import Alert from '@/components/Alert'
import PageHeader from '@/components/PageHeader'
import Loading from '@/components/Loading'
import Message from '@/components/Message'
import MessageBox from '@/components/MessageBox'
import Tabs from '@/components/Tabs'
import Tree from '@/components/Tree'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      redirect: "/layout",
      children: [
        {
          path: '/layout',
          name: 'Layout',
          component: Layout
        },
        {
          path: '/button',
          name: 'Button',
          component: Button
        },
        {
          path: '/form',
          name: 'Form',
          component: Form
        },
        {
          path: '/icon',
          name: 'Icon',
          component: Icon
        },
        {
          path: '/input',
          name: 'Input',
          component: Input
        },
        {
          path: '/radio',
          name: Radio,
          component: Radio
        },
        {
          path: '/checkbox',
          name: Checkbox,
          component: Checkbox
        },
        {
          path: '/switch',
          name: Switch,
          component: Switch
        },
        {
          path: '/select',
          name: Select,
          component: Select
        },
        {
          path: "/cascader",
          name: Cascader,
          component: Cascader
        },
        {
          path: "/datepicker",
          name: DatePicker,
          component: DatePicker
        },
        {
          path: "/validator",
          name: Validator,
          component: Validator
        },
        {
          path: "form2",
          name: Form2,
          component: Form2
        },
        {
          path: "form3",
          name: Form3,
          component: Form3
        },
        {
          path: "table",
          name: Table,
          component: Table
        },
        {
          path: "pagination",
          name: Pagination,
          component: Pagination
        },
        {
          path: "dialog",
          name: Dialog,
          component: Dialog
        },
        {
          path: "card",
          name: Card,
          component: Card
        },
        {
          path: "breadcrumb",
          name: Breadcrumb,
          component: Breadcrumb
        },
        {
          path: "steps",
          name: Steps,
          component: Steps
        },
        {
          path: "navmenu",
          name: NavMenu,
          component: NavMenu
        },
        {
          path: "avatar",
          name: Avatar,
          component: Avatar
        },
        {
          path: "dropdown",
          name: Dropdown,
          component: Dropdown
        },
        {
          path:"alert",
          name:Alert,
          component:Alert
        },
        {
          path:"pageheader",
          name:PageHeader,
          component:PageHeader
        },
        {
          path:"loading",
          name:Loading,
          component:Loading
        },
        {
          path:"message",
          name:Message,
          component:Message
        },
        {
          path:"messagebox",
          name:MessageBox,
          component:MessageBox
        },
        {
          path:"tabs",
          name:Tabs,
          component:Tabs
        },
        {
          path:"tree",
          name:Tree,
          component:Tree
        }
      ]
    }

  ]
})
